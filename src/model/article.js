/**
 * 爬虫配置模型
 */

/*
const monk = require('monk')
const {
  DB: { url, connectOption },
} = require('../config')
const db = monk(url, connectOption)
const collection = db.get('crawl')
*/




const monk = require('monk')

// Connection URL  db.changeUserPassword('opsAdmin','newpassword');
const url = 'localhost:27017/crawl';

const db = monk(url);

/*db.then(() => {
  console.log('Connected correctly to server')
})*/
const collection = db.get('article')

/**
 * 爬虫类
 * @param {string} cid 当前爬虫配置id
 * @param {string} uid 用户id
 * @param {object} config 爬虫配置
 * @param {boolean} permission 配置权限,权限为true，则可以展示到分享页面
 * @param {number} interval 爬虫结果更新间隔时间
 * @param {object} result 爬虫抓取结果,对象内部 time 表示 value 更新时间
 * @param {string} time 配置创建时间
 */
class Article {
  /** 构建爬虫模型 */
  constructor({aid, content, title , img , author , publishTime , origin}) {
    this.aid = aid
    this.content = content
    this.title = title
    this.img = img
    this.author  = author
    this.publishTime = publishTime
    this.time = new Date().toLocaleString()
    this.origin = origin
  }

  /** 保存文章 */
  save() {
    const article = {
      content: this.content,
      title: this.title,
      img: this.img,
      author: this.author,
      publishTime: this.publishTime,
      time: this.time,
      origin: this.origin
    }

    return collection
      .insert(article)
      .then( function(docs) {
        console.info('message===' + docs.toString());
      })
      .catch(function(err) {
        console.info('the err message===' + err);
      })
  }

}

module.exports = Article
